Unity Project - 2D Logic Maze Game
"Bear Brothers"

Developed by: Nazar Mamedov

**Description**

In current times, the demand for engaging and intellectually stimulating games has remained very
high. "Bear Brothers" is my contribution to the ever growing and evolving puzzle game genre,
offering an immersive, challenging and entertaining puzzle game experience.
The game puts the highest priority on the user experience. The gameplay environments are specif-
ically designed to be stimulating, yet digestible, the gameplay mechanics are intricate, yet simple
and the overall navigation is both intuitive and rapid.
Key features of "Bear Brothers" include specifically crafted level layouts, innovative character
control and captivating story and design elements, all of which contribute to an engaging and
challenging gameplay environment. This environment is meant to be enjoyed by a wide audience,
from young students seeking mental stimulation to hardened logic puzzle enthusiasts.
See "Bear_Brothers_Documentation.pdf" report for an in-depth documentation of "Bear Brothers" game, providing details and in-
sights into its development process, relevant technologies and the principles forming its design. In
summary, "Bear Brothers" is not just a fun game, it is an intellectual challenge for players of all
backgrounds, allowing exploration, engagement, and conquest of intricate puzzle maps.


**How to Open and Compile the Unity Project**

Prerequisites:
- Unity Hub installed
- Unity version compatible with the project

Instructions:
- Clone the repository to your local machine: `git clone https://gitlab.com/NazarMam/brother-bears-2d-puzzle-game.git`
- Open Unity Hub and click on "Add" to add the project to your Unity Hub projects.
- Navigate to the project folder and select the root directory of your Unity project.
- Choose the correct Unity version if prompted, and click "Open."
- Once the project is added, select it in Unity Hub and click "Open Project."
- Unity will now load the project, and you can explore, modify, and compile the project as needed.

**How to Execute the Game**

- Navigate to the Builds directory within the project.
- Unzip the Builds directory.
- Find the executable file (NM Bachelors Project.exe) in the build directory.
- Double-click the executable file to run the game.
