using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DefaultExecutionOrder(0)]
public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 1.0f;
    public MapGenerator mapGenerator;
    public VictoryScreen victoryScreen;
    public PlayerRotation PlayerRotation;

    private Vector3 currentGridPosition;
    public bool isMoving = false;

    public Vector3 direction { get; private set; }
    public Vector3 playerRotation { get; private set; }
    private Vector3 frontDirection;
    private LevelData levelData;
    public bool isResetting = false;
    
    // Statistics for levels
    private int currRotations=0;
    private float currTime=0f;
    private float timer;
    private bool isTimerRunning=false;
    private bool isWinning = false;
    public Text timerText;
    private bool isPaused = false;

    //Skins
    public GameObject playerSprite;
    public GameObject playerSpriteV2;
    public GameObject playerSpriteV3;

    //Tooltips for time-pause
    public GameObject ToolTipRotateScreen;
    public GameObject ToolTipEnemiesScreen;
    public GameObject ToolTipBowEnemiesScreen;

    void Start()
    {
        levelData = mapGenerator.GetCurrentLevelData();
        frontDirection = transform.up;
        BoxCollider2D playerCollider = GetComponent<BoxCollider2D>();

        int currentSkin = PlayerPrefs.GetInt("skin", 0);
        SetSpritesVisibility(currentSkin);

        Vector3 cornerPosition = new Vector3(1f, 1f, 0f);
        transform.position = cornerPosition;
        currentGridPosition = cornerPosition;

        StartTimer();
    }

    void Update()
    {
        if(ToolTipRotateScreen.activeSelf || ToolTipEnemiesScreen.activeSelf || ToolTipBowEnemiesScreen.activeSelf)
        {
            isPaused = true;
        }
        else
        {
            isPaused = false;
        }

        timerText.text = timer.ToString("F2") + "s";
        playerRotation = transform.eulerAngles;

        if (isTimerRunning && !isPaused)
        {
            timer += Time.deltaTime;
        }

        if (!isMoving && Input.anyKeyDown)
        {
            direction = Vector3.zero;

            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            {
                direction = -frontDirection; // Backwards.
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                direction = frontDirection; // Front.
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            {
                direction = -Vector3.Cross(frontDirection, Vector3.forward); // Left.
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            {
                direction = Vector3.Cross(frontDirection, Vector3.forward); // Right.
            }

            if (direction != Vector3.zero)
            {
                Vector3 newGridPosition = currentGridPosition + direction;

                if (!HasCollision(newGridPosition))
                {
                    StartCoroutine(MoveToGridPosition(newGridPosition));
                }
                else
                {
                    StartCoroutine(BounceBackMove(direction));
                }
            }
        }
    }


    private IEnumerator MoveToGridPosition(Vector3 targetPosition)
    {
        isMoving = true;

        if (isResetting==false)
        {
            while (Vector3.Distance(transform.position, targetPosition) > 0.01f)
            {
                if (isResetting)
                {
                    yield break;
                }
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
                yield return null;
            }
        }

        transform.position = targetPosition;
        currentGridPosition = targetPosition;
        isMoving = false;
    }

    private bool HasCollision(Vector3 targetPosition)
    {
        int cellX = Mathf.FloorToInt(targetPosition.x);
        int cellY = Mathf.FloorToInt(targetPosition.y);

        char cellType = levelData.map[levelData.height - cellY - 1][cellX];

        if (cellType == 'X')
        {
            return true;
        }
        else if (cellType == 'W')
        {
            StopTimer();
            isWinning = true;
            isResetting = true;
            //currRotations = PlayerRotation.rotations;
            //currTime = timer;
            //victoryScreen.StatReport(levelData.id, true, currRotations, currTime);
            return false;
        }

        int adjacentCellX = Mathf.FloorToInt(targetPosition.x);
        int adjacentCellY = Mathf.FloorToInt(targetPosition.y);

        playerRotation = transform.eulerAngles;

        if (playerRotation.z > 45 && playerRotation.z < 135)
        {
            // Facing right
            // Moving Up
            if (direction == frontDirection)
            {
                adjacentCellX += 1;
            }
            // Moving Down
            else if (direction == -frontDirection)
            {
                adjacentCellX += 1;
            }
            // Moving Right
            else if (direction == Vector3.Cross(frontDirection, Vector3.forward))
            {
                adjacentCellX += 1;
            }
            // Moving Left
            else
            {
                // will get detected by upper function
            }
        }
        else if (playerRotation.z > 135 && playerRotation.z < 225)
        {
            // Facing up
            // Moving Up
            if (direction == frontDirection)
            {
                adjacentCellY += 1;
            }
            // Moving Down
            else if (direction == -frontDirection)
            {
                // will get detected by upper function
            }
            // Moving Right
            else if (direction == Vector3.Cross(frontDirection, Vector3.forward))
            {
                adjacentCellY += 1;
            }
            // Moving Left
            else
            {
                adjacentCellY += 1;
            }
        }
        else if (playerRotation.z > 225 && playerRotation.z < 315)
        {
            // Facing left
            // Moving Up
            if (direction == frontDirection)
            {
                adjacentCellX -= 1;
            }
            // Moving Down
            else if (direction == -frontDirection)
            {
                adjacentCellX -= 1;
            }
            // Moving Right
            else if (direction == Vector3.Cross(frontDirection, Vector3.forward))
            {
                // will get detected by upper function
            }
            // Moving Left
            else
            {
                adjacentCellX -= 1;
            }
        }
        else if (playerRotation.z < 45)
        {
            // Facing down
            // Moving Up
            if (direction == frontDirection)
            {
                // will get detected by upper function
            }
            // Moving Down
            else if (direction == -frontDirection)
            {
                adjacentCellY -= 1;
            }
            // Moving Right
            else if (direction == Vector3.Cross(frontDirection, Vector3.forward))
            {
                adjacentCellY -= 1;
            }
            // Moving Left
            else
            {
                adjacentCellY -= 1;
            }
        }
        else
        {
            Debug.LogWarning("Angle error.");
        }

        char adjacentCellType = levelData.map[levelData.height - adjacentCellY - 1][adjacentCellX];

        if (adjacentCellType == 'X')
        {
            return true;
        }
        else if (adjacentCellType == 'W')
        {
            StopTimer();
            isResetting = true;
            isWinning = true;
            //currRotations = PlayerRotation.rotations;
            //currTime = timer;
            //victoryScreen.StatReport(levelData.id, true, currRotations, currTime);
            return false;
        }
        return false;
    }

    private IEnumerator BounceBackMove(Vector3 direction)
    {
        float t = 0;
        float bounceDistance = 0.5f;

        Vector3 startPosition = transform.position;
        Vector3 endPosition = startPosition + (direction.normalized * bounceDistance);

        while (t < 1)
        {
            t += 20f * Time.deltaTime;
            transform.position = Vector3.Lerp(startPosition, endPosition, t);
            yield return null;
        }

        float returnDistance = Vector3.Distance(transform.position, startPosition);
        float returnSpeed = 30f;
        while (returnDistance > 0)
        {
            float step = returnSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, startPosition, step);
            returnDistance = Vector3.Distance(transform.position, startPosition);
            yield return null;
        }
    }

    public void StartTimer()
    {
        timer = 0f;
        isTimerRunning = true;
    }

    public void StopTimer()
    {
        isTimerRunning = false;
    }

    public void ResetPlayer()
    {
        currRotations = PlayerRotation.rotations;
        currTime = timer;
        victoryScreen.StatReport(levelData.id, isWinning, currRotations, currTime);
        Debug.Log("Level - " + levelData.id + ", Win? " + isWinning + ", rotations - " + currRotations + ", time - " + currTime + ".");
        isWinning = false;

        isResetting = true;
        isMoving=true;
        levelData = mapGenerator.GetCurrentLevelData();
        Vector3 resetPosition = new Vector3(1f, 1f, 0f);
        transform.position = resetPosition;
        currentGridPosition = resetPosition;
        isMoving=false;
        StartCoroutine(ResetPlayerCoroutine(resetPosition));
    }

    private IEnumerator ResetPlayerCoroutine(Vector3 targetPosition)
    {
        yield return StartCoroutine(MoveToGridPosition(targetPosition));
        isResetting = false;
        StartTimer();
    }

    void SetSpritesVisibility(int selectedSkin)
    {
        switch (selectedSkin)
        {
            case 0:
                ActivateSprite(playerSprite);
                DeactivateSprite(playerSpriteV2);
                DeactivateSprite(playerSpriteV3);
                break;
            case 1:
                ActivateSprite(playerSpriteV2);
                DeactivateSprite(playerSprite);
                DeactivateSprite(playerSpriteV3);
                break;
            case 2:
                ActivateSprite(playerSpriteV3);
                DeactivateSprite(playerSpriteV2);
                DeactivateSprite(playerSprite);
                break;
            default:
                Debug.LogError("Invalid skin value");
                break;
        }
    }

    void ActivateSprite(GameObject spriteObject)
    {
        spriteObject.SetActive(true);
    }

    void DeactivateSprite(GameObject spriteObject)
    {
        spriteObject.SetActive(false);
    }
}
