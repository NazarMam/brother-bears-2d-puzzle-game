using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;

public class DatabaseManager : MonoBehaviour
{
    private int rotateCount = 0;
    private int invalidRotateCount = 0;

    void Start()
    {
        IDbConnection dbConnection = CreateAndOpenDatabase();

        InsertLevelLayout(0, "Training Grounds I", 9, 5, "XXXXXXXXXXYYXYYYWXXYYYYYXYXXYYXYYYYXXXXXXXXXX", dbConnection);
        InsertLevelLayout(1, "Training Grounds II", 9, 5, "XXXXXXXXXXYSXXXXWXXYYYYYYYXXYYYXYYYXXXXXXXXXX", dbConnection);
        InsertLevelLayout(2, "Training Grounds III", 10, 6, "XXXXXXXXXXXYYYYYYXWXXYYYYYYXYXXYXYYBYYYXXYXYYYYYYXXXXXXXXXXX", dbConnection);
        InsertLevelLayout(3, "Guarded gardens", 9, 7, "XXXXXXXXXXYSYYXYWXXYYYYYYYXXXXYSXXXXXYYYYYYYXXYYYYYSYXXXXXXXXXX", dbConnection);
        InsertLevelLayout(4, "Entrance into the tunnel", 9, 7, "XXXXXXXXXXYYSYYYYXXYYYSXYYXXYYYYYYBXXYYYYXYYXXYYYYXYWXXXXXXXXXX", dbConnection);
        InsertLevelLayout(5, "Entrance of an encampment", 11, 8, "XXXXXXXXXXXXYSXYYXBXXXXYYYSYYYYXXXBYYYYYYYWXXXYYXYYYYXXXYYYYYYXBXXXYYYYYYYXXXXXXXXXXXXXX", dbConnection);
        InsertLevelLayout(6, "Entrance of an encampment", 11, 8, "XXXXXXXXXXXXBSYYYSYYYXXYYYYXYSYYXXWBYYXBYYYXXXXXXXXXSYXXYYYYSYYYYXXYYYSYYYYSXXXXXXXXXXXX", dbConnection);
        InsertLevelLayout(7, "Prison", 15, 11, "XXXXXXXXXXXXXXXXBXSYYYYYBXXYYXXXYYYYYYBXYYYXXXSYYSYYYXYYYXXXXYYYYXYYYYYXBWXXWYBXYYYYYXYYYXXXXXYYYXYYXYYYXXYYSYYXYYYSYYYXXYYYXXSYYYYSYYXXYYXSXYYYYBYYSXXXXXXXXXXXXXXXX", dbConnection);
        InsertLevelLayout(8, "Entrance of an encampment", 15, 11, "XXXXXXXXXXXXXXXXYYYBXYYYYBYYYXXYYYYYYYYYYYYYXXYYYSXYYSYSSYYXXYYYXXXXXXXYYYXXYYYYXYWYXYSYSXXYYYBXYYYXBYYYXXYYYYSYSYXYYXXXXYYYYSYYYXYYYBXXYYYYSYYYYYYSYXXXXXXXXXXXXXXXX", dbConnection);
        InsertLevelLayout(9, "Entrance of an encampment", 15, 11, "XXXXXXXXXXXXXXXXYBYYXYYYXSYBYXXYYYYSSYYXYYYYXXYYYXYYXYYSYYYXXWYYXBYBYYXYYYXXXXXXXXXXXYYSXXXYYYYYYYXYYYSXXXYYYSYYYXYYSYYXXYYYXBYYXYYYYYXXYYYXSYBYYYXYYXXXXXXXXXXXXXXXX", dbConnection);
        InsertLevelLayout(10, "Entrance of an encampment", 15, 11, "XXXXXXXXXXXXXXXXYBYYYYYYYYSYYXXYYYSYYXXYBXYYXXXYBYYYYSYYXWXXXSYXXYYYYSYXXYXXYBYXXYYYXXYYYXXXYXYYXYXYYYXYXXYYYYYSSYYYYYBXXYYYYYYYYYYYXYXXYYYYYYYYYYYYYXXXXXXXXXXXXXXXX", dbConnection);

        IDbCommand dbCommandReadValues = dbConnection.CreateCommand();
        dbCommandReadValues.CommandText = "SELECT actionType, hits FROM StatisticsTable WHERE actionType = 'Rotations' OR actionType = 'Inv. Rotations'";
        IDataReader dataReader = dbCommandReadValues.ExecuteReader();

        while (dataReader.Read())
        {
            string actionType = dataReader.GetString(0);
            int hits = dataReader.GetInt32(1);

            if (actionType == "Rotations")
            {
                rotateCount = hits;
            }
            else if (actionType == "Inv. Rotations")
            {
                invalidRotateCount = hits;
            }
        }

        dbConnection.Close();
    }

    public void OnRotate()
    {
        rotateCount++;

        IDbConnection dbConnection = CreateAndOpenDatabase();
        IDbCommand dbCommandInsertValue = dbConnection.CreateCommand();
        string addRotQuery =  "INSERT OR REPLACE INTO StatisticsTable (id, actionType, hits) VALUES (0, 'Rotations', " + rotateCount + ")";
        ExecuteQuery(addRotQuery, dbConnection);

        dbConnection.Close();
    }

    public void OnInvalidRotate()
    {
        invalidRotateCount++;

        IDbConnection dbConnection = CreateAndOpenDatabase();
        IDbCommand dbCommandInsertValue = dbConnection.CreateCommand();
        string addInvRotQuery =  "INSERT OR REPLACE INTO StatisticsTable (id, actionType, hits) VALUES (1, 'Inv. Rotations', " + invalidRotateCount + ")";
        ExecuteQuery(addInvRotQuery, dbConnection);

        dbConnection.Close();
    }

    private void InsertLevelLayout(int ID, string title, int width, int height, string map, IDbConnection dbConnection)
    {
        string insertQuery = $"INSERT OR REPLACE INTO LevelLayouts (ID, title, width, height, map) VALUES " +
                             $"({ID}, '{title}', {width}, {height}, '{map}')";
        ExecuteQuery(insertQuery, dbConnection);
    }

    public LevelData GetLevelData(int levelNumber)
    {
        LevelData levelData = new LevelData();

        IDbConnection dbConnection = CreateAndOpenDatabase();
        IDbCommand dbCommandReadValues = dbConnection.CreateCommand();
        dbCommandReadValues.CommandText = $"SELECT ID, width, height, map FROM LevelLayouts WHERE ID = {levelNumber}";
        IDataReader dataReader = dbCommandReadValues.ExecuteReader();

        while (dataReader.Read())
        {
            levelData.id = dataReader.GetInt32(0);
            levelData.width = dataReader.GetInt32(1);
            levelData.height = dataReader.GetInt32(2);
            string mapString = dataReader.GetString(3);
            levelData.map = ConvertMapStringToList(mapString, levelData.width);
        }

        dbConnection.Close();
        return levelData;
    }

    private List<string> ConvertMapStringToList(string mapString, int width)
    {
        List<string> levelMap = new List<string>();

        for (int i = 0; i < mapString.Length; i+=width)
        {
            string mapRow = mapString.Substring(i, width);
            levelMap.Add(mapRow);
        }

        return levelMap;
    }

    private IDbConnection CreateAndOpenDatabase()
    {
        string dbUri = $"URI=file:" + Application.dataPath + "/DatabaseFile.sqlite";
        IDbConnection dbConnection = new SqliteConnection(dbUri);
        dbConnection.Open();

        IDbCommand dbCommandCreateTable = dbConnection.CreateCommand();
        dbCommandCreateTable.CommandText = "CREATE TABLE IF NOT EXISTS StatisticsTable (id INTEGER PRIMARY KEY, actionType STRING, hits INTEGER )";
        dbCommandCreateTable.ExecuteReader();
        string createLayoutTableQuery = @"CREATE TABLE IF NOT EXISTS LevelLayouts (
                                    ID INTEGER PRIMARY KEY,
                                    title STRING,
                                    width INTEGER,
                                    height INTEGER,
                                    map STRING
                                )";
        ExecuteQuery(createLayoutTableQuery, dbConnection);

        return dbConnection;
    }
    private void ExecuteQuery(string query, IDbConnection dbConnection)
    {
        IDbCommand dbCommand = dbConnection.CreateCommand();
        dbCommand.CommandText = query;
        dbCommand.ExecuteNonQuery();
    }
}
