using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class LevelData
{
    public int id;
    public int width;
    public int height;
    public List<string> map;
}

[System.Serializable]
public class MapData
{
    public List<LevelData> levels;
}

[DefaultExecutionOrder(-1)]
public class MapGenerator : MonoBehaviour
{
    public GameObject mapContainer;
    public Camera mainCamera;
    public GameObject wallPrefab;
    public GameObject floorPrefab;
    public GameObject victoryTile;
    public GameObject swordmanPrefab;
    public GameObject bowmanPrefab;
    public DatabaseManager databaseManager;
    public GameObject ToolTipRotateScreen;
    public GameObject ToolTipEnemiesScreen;
    public GameObject ToolTipBowEnemiesScreen;
    public int levelNum=4;

    public LevelData currentLevelData;

    public Text levelText;

    void Start()
    {
        levelNum = PlayerPrefs.GetInt("SelectedLevel", 1);
        Screen.SetResolution(1920, 1080, true);
        ToolTipRotateScreen.SetActive(false);
        ToolTipEnemiesScreen.SetActive(false);
        ToolTipBowEnemiesScreen.SetActive(false);
        LoadAndGenerateMap(levelNum);
    }

    void Update()
    {
        levelText.text = (levelNum-1).ToString();

        if (Input.anyKeyDown)
        {
            ToolTipRotateScreen.SetActive(false);
            ToolTipEnemiesScreen.SetActive(false);
            ToolTipBowEnemiesScreen.SetActive(false);
        }
    }

    public void LoadAndGenerateMap(int levelNumber)
    {

        if(levelNumber>10)
        {
            SceneManager.LoadScene("MainMenu");
        }
        else if(levelNumber==0)
        {
            ToolTipRotateScreen.SetActive(true);
            StartCoroutine(FloatToolTipScreen(ToolTipRotateScreen));
        }
        else if(levelNumber==1)
        {
            ToolTipEnemiesScreen.SetActive(true);
            StartCoroutine(FloatToolTipScreen(ToolTipEnemiesScreen));
        }
        else if(levelNumber==2)
        {
            ToolTipBowEnemiesScreen.SetActive(true);
            StartCoroutine(FloatToolTipScreen(ToolTipBowEnemiesScreen));
        }

        currentLevelData = databaseManager.GetLevelData(levelNumber);
        CenterAndFitCamera();

        levelNum++;

        for (int y = currentLevelData.height - 1; y >= 0; y--)
        {
            string row = currentLevelData.map[y];

            for (int x = 0; x < currentLevelData.width; x++)
            {
                char cellType = row[x];
                Vector3 position = new Vector3(x, currentLevelData.height - 1 - y, 0);

                if (cellType == 'X')
                {
                    Instantiate(floorPrefab, position, Quaternion.identity, mapContainer.transform);
                    Instantiate(wallPrefab, position, Quaternion.identity, mapContainer.transform);
                }
                else if (cellType == 'Y')
                {
                    Instantiate(floorPrefab, position, Quaternion.identity, mapContainer.transform);
                }
                else if (cellType == 'W')
                {
                    Instantiate(floorPrefab, position, Quaternion.identity, mapContainer.transform);
                    Instantiate(victoryTile, position - new Vector3(0.0f, 0.5f, 0.0f), Quaternion.identity, mapContainer.transform);
                }
                else if (cellType == 'S')
                {
                    Instantiate(floorPrefab, position, Quaternion.identity, mapContainer.transform);
                    Instantiate(swordmanPrefab, position, Quaternion.identity, mapContainer.transform);
                }
                else if (cellType == 'B')
                {
                    Instantiate(floorPrefab, position, Quaternion.identity, mapContainer.transform);
                    Instantiate(bowmanPrefab, position, Quaternion.identity, mapContainer.transform);
                }
            }
        }
    }

    public void ResetLevel()
    {
        ClearLevel();
        LoadAndGenerateMap(levelNum);
    }

    public void ReloadLevel()
    {
        levelNum=levelNum-1;

        ClearLevel();
        LoadAndGenerateMap(levelNum);
    }

    public int GetLevelID()
    {
        return currentLevelData.id;
    }

    public void ClearLevel()
    {
        foreach (Transform child in mapContainer.transform)
        {
            Destroy(child.gameObject);
        }
    }

    void CenterAndFitCamera()
    {
        Vector3 mapCenter = new Vector3((currentLevelData.width - 1) * 1.0f / 2f, (currentLevelData.height) * 1.0f / 2f, 0f);
        float cameraSize = Mathf.Max(currentLevelData.width, currentLevelData.height) * 1.2f / 2f;

        if(levelNum>2)
        cameraSize *= 1.5f;

        mainCamera.transform.position = new Vector3(mapCenter.x, mapCenter.y, -cameraSize);
        mainCamera.transform.LookAt(mapCenter);
    }

    IEnumerator FloatToolTipScreen(GameObject toolTip)
    {
        float animationDuration = 1.0f;
        Vector3 targetPosition = toolTip.transform.localPosition;
        Vector3 bottomPosition = new Vector3(targetPosition.x, targetPosition.y - 10f, targetPosition.z);
        float elapsed = 0f;

        while (elapsed < animationDuration)
        {
            float t = elapsed / animationDuration;

            toolTip.transform.localPosition = Vector3.Lerp(bottomPosition, targetPosition, t);

            elapsed += Time.deltaTime;
            yield return null;
        }

        toolTip.transform.localPosition = targetPosition;
    }

    public LevelData GetCurrentLevelData()
    {
        return currentLevelData;
    }
}
