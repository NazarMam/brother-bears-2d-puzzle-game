using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectManager : MonoBehaviour
{
    public Camera mainCamera;
    float cameraMoveSpeed = 30f;
    Vector3 originalCameraPosition;

    private LevelSelectDatabaseManager databaseManager;

    void Start()
    {
        originalCameraPosition = mainCamera.transform.position;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }

        if (Input.mousePosition.x < Screen.width * 0.1f)
        {
            MoveCamera(-1); // Slide screen left
        }
        else if (Input.mousePosition.x > Screen.width * 0.9f)
        {
            MoveCamera(1); // Slide screen right
        }
    }

    void MoveCamera(int direction)
    {
        float newX = mainCamera.transform.position.x + direction * cameraMoveSpeed * Time.deltaTime;
        newX = Mathf.Clamp(newX, originalCameraPosition.x, 176f);

        mainCamera.transform.position = new Vector3(newX, mainCamera.transform.position.y, mainCamera.transform.position.z);
    }
}
