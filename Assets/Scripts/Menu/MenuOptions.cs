using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuOptions : MonoBehaviour
{
    public string menuOption;
    public GameObject OptionScreen;

    void Start()
    {
        OptionScreen.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OptionScreen.SetActive(false);
        }
    }

    void OnMouseDown()
    {
        if(menuOption == "Play")
        {
            SceneManager.LoadScene("LevelSelect");
        }
        else if(menuOption == "Options")
        {
            OptionScreen.SetActive(true);
        }
        else if(menuOption == "Quit")
        {
            PlayerPrefs.SetInt("Intro", 1);
            Application.Quit();
        }
    }

    void OnMouseEnter()
    {
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.7f);
    }

    void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
    }
}