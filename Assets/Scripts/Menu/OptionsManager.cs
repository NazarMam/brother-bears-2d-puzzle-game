using UnityEngine;

public class OptionsManager : MonoBehaviour
{
    public int selectedSkinIndex;
    private int selectedSkin;
    public GameObject chosenIndicatorPrefab;

    private GameObject chosenIndicator;

    void Start()
    {
        // Find all existing chosen indicators with the "ChosenIndicator" tag
        GameObject[] existingChosenIndicators = GameObject.FindGameObjectsWithTag("ChosenIndicator");

        // Check if the collection is not empty
        if (existingChosenIndicators != null && existingChosenIndicators.Length > 0)
        {
            // Existing indicators found, no need to instantiate
            return;
        }

        // No existing indicators found, use PlayerPrefs to determine the selected skin
        Debug.Log("Initial skin choice - " + selectedSkin);
        selectedSkin = PlayerPrefs.GetInt("skin", 2);
        Debug.Log("Final skin choice - " + selectedSkin);
        
        switch (selectedSkin)
        {
            case 0:
                chosenIndicator = Instantiate(chosenIndicatorPrefab, transform.position + new Vector3(-26.81f, 5f, 0f), Quaternion.identity);
                break;
            case 1:
                chosenIndicator = Instantiate(chosenIndicatorPrefab, transform.position + new Vector3(-13.04f, 5f, 0f), Quaternion.identity);
                break;
            case 2:
                chosenIndicator = Instantiate(chosenIndicatorPrefab, transform.position + new Vector3(0f, 5f, 0f), Quaternion.identity);
                break;
            default:
                break;
        }

        chosenIndicator.transform.localScale = new Vector3(2f, 2f, 1f);
        chosenIndicator.tag = "ChosenIndicator";

    }

    void OnMouseDown()
    {
        // Set the selected skin in PlayerPrefs
        PlayerPrefs.SetInt("skin", selectedSkinIndex);

        // Update the chosen indicator
        UpdateChosenIndicator(selectedSkinIndex);
    }

    void OnMouseEnter()
    {
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.8f);
    }

    void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.29f);
    }

    void UpdateChosenIndicator(int selectedSkin)
    {
        // Destroy all previously instantiated chosen indicators
        GameObject[] existingChosenIndicators = GameObject.FindGameObjectsWithTag("ChosenIndicator");
        foreach (var existingIndicator in existingChosenIndicators)
        {
            Destroy(existingIndicator);
        }

        // Instantiate the chosen indicator for the selected skin
        chosenIndicator = Instantiate(chosenIndicatorPrefab, transform.position + Vector3.up * 5f, Quaternion.identity);
        chosenIndicator.transform.localScale = new Vector3(2f, 2f, 1f);
        chosenIndicator.tag = "ChosenIndicator";
    }
}
