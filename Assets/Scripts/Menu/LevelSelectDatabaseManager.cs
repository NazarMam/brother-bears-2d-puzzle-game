using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;

public class LevelSelectDatabaseManager : MonoBehaviour
{
    void Start()
    {
        IDbConnection dbConnection = CreateAndOpenDatabase();

        if (!HasExistingData(dbConnection))
        {
            InsertLevelStatistics(0, 0, true, 0f, 0, dbConnection);
            InsertLevelStatistics(1, 1, false, 0f, 0, dbConnection);
            InsertLevelStatistics(2, 2, false, 0f, 0, dbConnection);
            InsertLevelStatistics(3, 3, false, 0f, 0, dbConnection);
            InsertLevelStatistics(4, 4, false, 0f, 0, dbConnection);
            InsertLevelStatistics(5, 5, false, 0f, 0, dbConnection);
            InsertLevelStatistics(6, 6, false, 0f, 0, dbConnection);
            InsertLevelStatistics(7, 7, false, 0f, 0, dbConnection);
            InsertLevelStatistics(8, 8, false, 0f, 0, dbConnection);
            InsertLevelStatistics(9, 9, false, 0f, 0, dbConnection);
            InsertLevelStatistics(10, 10, false, 0f, 0, dbConnection);
        }

        dbConnection.Close();
    }

    private void InsertLevelStatistics(int ID, int levelID, bool completion, float bestTime, int rotations, IDbConnection dbConnection)
    {
        string insertQuery = $"INSERT OR REPLACE INTO LevelStatistics (ID, levelID, completion, bestTime, rotations) VALUES " +
                             $"({ID}, '{levelID}', {completion}, {bestTime}, '{rotations}')";
        ExecuteQuery(insertQuery, dbConnection);
    }

    public void UpdateLevelStatistics(int levelID, bool completion, int currRotations, float currTime)
    {
        Debug.Log("UPDATING - Level - " + levelID + ", Win? " + completion + ", rotations - " + currRotations + ", time - " + currTime + ".");

        try
        {
            string dbUri = $"URI=file:" + Application.dataPath + "/DatabaseFile.sqlite";
            using (IDbConnection dbConnection = new SqliteConnection(dbUri))
            {
                dbConnection.Open();

                // COMPLETION
                if (completion)
                {
                    string updateCompletionQuery = $"UPDATE LevelStatistics SET completion = 1 WHERE levelID = {levelID}";
                    ExecuteNonQuery(updateCompletionQuery, dbConnection);
                }

                // TOTAL ROTATIONS
                string selectRotationsQuery = $"SELECT rotations FROM LevelStatistics WHERE levelID = {levelID}";
                int retrievedRotations = ExecuteQuery<int>(selectRotationsQuery, dbConnection);
                int newTotalRotations = retrievedRotations + currRotations;
                string updateRotationsQuery = $"UPDATE LevelStatistics SET rotations = {newTotalRotations} WHERE levelID = {levelID}";
                ExecuteNonQuery(updateRotationsQuery, dbConnection);

                // BEST TIME
                string selectBestTimeQuery = $"SELECT bestTime FROM LevelStatistics WHERE levelID = {levelID}";
                float retrievedBestTime = ExecuteQuery<float>(selectBestTimeQuery, dbConnection);
                float roundedCurrTime = Mathf.Round(currTime * 100) / 100f;
                if ((roundedCurrTime < retrievedBestTime && currTime != 0) || retrievedBestTime == 0)
                {
                    string updateBestTimeQuery = $"UPDATE LevelStatistics SET bestTime = {roundedCurrTime} WHERE levelID = {levelID}";
                    ExecuteNonQuery(updateBestTimeQuery, dbConnection);
                    Debug.Log($"Update Best Time Query: {updateBestTimeQuery}");
                }
            }
        }
        catch (Exception e)
        {
            // Log the exception details. This is crucial for debugging.
            Debug.LogError($"Error in database operations: {e.Message}\n{e.StackTrace}");
        }

        //string dbUri = $"URI=file:" + Application.dataPath + "/DatabaseFile.sqlite";
        //IDbConnection dbConnection = new SqliteConnection(dbUri);
        //dbConnection.Open();

        // COMPLETION
        //if (completion)
        //{
            //string updateCompletionQuery = $"UPDATE LevelStatistics SET completion = 1 WHERE levelID = {levelID}";
            //ExecuteNonQuery(updateCompletionQuery, dbConnection);
        //}

        // TOTAL ROTATIONS
        //string selectRotationsQuery = $"SELECT rotations FROM LevelStatistics WHERE levelID = {levelID}";
        //int retrievedRotations = ExecuteQuery<int>(selectRotationsQuery, dbConnection);
        //int newTotalRotations = retrievedRotations + currRotations;
        //string updateRotationsQuery = $"UPDATE LevelStatistics SET rotations = {newTotalRotations} WHERE levelID = {levelID}";
        //ExecuteNonQuery(updateRotationsQuery, dbConnection);

        // BEST TIME
        //string selectBestTimeQuery = $"SELECT bestTime FROM LevelStatistics WHERE levelID = {levelID}";
        //float retrievedBestTime = ExecuteQuery<float>(selectBestTimeQuery, dbConnection);
        //if ((currTime < retrievedBestTime && currTime!=0) || retrievedBestTime == 0)
        //{
            //string updateBestTimeQuery = $"UPDATE LevelStatistics SET bestTime = {currTime} WHERE levelID = {levelID}";
            //ExecuteNonQuery(updateBestTimeQuery, dbConnection);
        //}
    
        //dbConnection.Close();
    }
    
    public bool GetCompletionStatus(int levelID)
    {
        string dbUri = $"URI=file:" + Application.dataPath + "/DatabaseFile.sqlite";
        IDbConnection dbConnection = new SqliteConnection(dbUri);
        dbConnection.Open();

        string updateCompletionQuery = $"SELECT completion FROM LevelStatistics WHERE levelID = {levelID}";
        return ExecuteQuery<bool>(updateCompletionQuery, dbConnection);
        dbConnection.Close();
    }

    public int GetTotalRotations(int levelID)
    {
        string dbUri = $"URI=file:" + Application.dataPath + "/DatabaseFile.sqlite";
        IDbConnection dbConnection = new SqliteConnection(dbUri);
        dbConnection.Open();

        string selectRotationsQuery = $"SELECT rotations FROM LevelStatistics WHERE levelID = {levelID}";
        
        return ExecuteQuery<int>(selectRotationsQuery, dbConnection);
        dbConnection.Close();
    }

    public float GetBestTime(int levelID)
    {
        string dbUri = $"URI=file:" + Application.dataPath + "/DatabaseFile.sqlite";
        IDbConnection dbConnection = new SqliteConnection(dbUri);
        dbConnection.Open();

        string selectBestTimeQuery = $"SELECT bestTime FROM LevelStatistics WHERE levelID = {levelID}";
        
        return ExecuteQuery<float>(selectBestTimeQuery, dbConnection);
        dbConnection.Close();
    }

    private IDbConnection CreateAndOpenDatabase()
    {
        string dbUri = $"URI=file:" + Application.dataPath + "/DatabaseFile.sqlite";
        IDbConnection dbConnection = new SqliteConnection(dbUri);
        dbConnection.Open();

        string createLevelStatisticsTableQuery = @"CREATE TABLE IF NOT EXISTS LevelStatistics (
                                    ID INTEGER PRIMARY KEY,
                                    levelID INTEGER,
                                    completion BOOLEAN,
                                    bestTime REAL,
                                    rotations INTEGER
                                )";
        ExecuteQuery(createLevelStatisticsTableQuery, dbConnection);

        return dbConnection;
    }

    private void ExecuteQuery(string query, IDbConnection dbConnection)
    {
        IDbCommand dbCommand = dbConnection.CreateCommand();
        dbCommand.CommandText = query;
        dbCommand.ExecuteNonQuery();
    }

    private T ExecuteQuery<T>(string query, IDbConnection dbConnection)
    {
        IDbCommand dbCommand = dbConnection.CreateCommand();
        dbCommand.CommandText = query;

        object result = dbCommand.ExecuteScalar();

        if (result != null && result != DBNull.Value)
        {
            if (typeof(T) == typeof(int))
            {
                return (T)(object)Convert.ToInt32(result);
            }
            else if (typeof(T) == typeof(float))
            {
                return (T)(object)Convert.ToSingle(result);
            }
            else
            {
                return (T)result;
            }
        }
        else
        {
            return default(T);
        }
    }

    private void ExecuteNonQuery(string query, IDbConnection dbConnection)
    {
        IDbCommand dbCommand = dbConnection.CreateCommand();
        dbCommand.CommandText = query;
        dbCommand.ExecuteNonQuery();
    }

    private bool HasExistingData(IDbConnection connection)
    {
        using (IDbCommand dbCommand = connection.CreateCommand())
        {
            dbCommand.CommandText = "SELECT COUNT(*) FROM LevelStatistics";
            object result = dbCommand.ExecuteScalar();

            return Convert.ToInt32(result) > 0;
        }
    }
}
