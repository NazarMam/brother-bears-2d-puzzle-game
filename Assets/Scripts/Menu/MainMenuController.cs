using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class MainMenuController : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject Play;
    public GameObject Options;
    public GameObject Quit;
    private float cinematicX;
    private float cinematicY;
    public float initCamX;
    public float initCamY;
    public float initCamZ;
    public bool cinematicShowing = false;
    private int cinematicSlide=1;
    int intro = 1;
    
    void Start()
    {

        if (PlayerPrefs.HasKey("Volume"))
        {
            AudioListener.volume = PlayerPrefs.GetFloat("Volume", 1);
        }

        if (!PlayerPrefs.HasKey("Intro") || PlayerPrefs.GetInt("Intro") == 1)
        {
            PlayerPrefs.SetInt("Intro", 1);
            Play.SetActive(false);
            Options.SetActive(false);
            Quit.SetActive(false);
        }
        else
        {
            Play.SetActive(true);
            Options.SetActive(true);
            Quit.SetActive(true);
        }

        Screen.SetResolution(1920, 1080, true);
        
        Vector3 initCam = mainCamera.transform.position;
        float initCamX = initCam.x;
        float initCamY = initCam.y;
        float initCamZ = initCam.z;
    }

    void Update()
    {
        intro = PlayerPrefs.GetInt("Intro", 1);
        if(intro == 1)
        {
            if (Input.anyKeyDown && cinematicSlide != 0)
            {
                cinematicSlide++;
                cinematicShowing = false;

                if(cinematicSlide>=4)
                {
                    mainCamera.transform.position = new Vector3(initCamX, initCamY, initCamZ);
                    Play.SetActive(true);
                    Options.SetActive(true);
                    Quit.SetActive(true);
                    cinematicSlide = 0;
                    PlayerPrefs.SetInt("Intro", 0);
                }
            }

            if(cinematicSlide!=0 && cinematicShowing == false)
            {
                cinematicY=-26.16f;
                cinematicX=-40.54f;

                if(cinematicSlide==1)
                {
                    cinematicX=-40.54f;
                }
                else if(cinematicSlide==2)
                {
                    cinematicX=-15.87f;
                }
                else if(cinematicSlide==3)
                {
                    cinematicX=6.7f;
                }

                ShowIntroCinematic(cinematicX, cinematicY);
            }
        }
    }

    void ShowIntroCinematic(float cinematicX, float cinematicY)
    {
        cinematicShowing = true;
        float cameraSize = 7.5f;
        
        mainCamera.transform.position = new Vector3(cinematicX, cinematicY, -cameraSize);
    }
}