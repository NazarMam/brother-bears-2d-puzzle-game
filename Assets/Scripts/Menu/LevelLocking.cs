using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLocking : MonoBehaviour
{
    private BoxCollider2D myCollider;
    private SpriteRenderer childRenderer;
    private GameObject childLock;
    public LevelSelectDatabaseManager databaseManager;
    public int LevelID;

    public Sprite lockedSprite;
    public Sprite unlockedSprite;

    void Start()
    {
        myCollider = GetComponent<BoxCollider2D>();
        childRenderer = FindChildSpriteRenderer("levelSprite");
        childLock = transform.Find("levelSprite (1)").gameObject;

        bool completion = databaseManager.GetCompletionStatus(LevelID);
        
        if(completion)
            EnableCollider();
        else
            DisableCollider();
    }

    void DisableCollider()
    {
        if (myCollider != null)
        {
            myCollider.enabled = false;
            childRenderer.sprite = lockedSprite;
            childLock.SetActive(true);
        }
        else
        {
            Debug.LogError("Collider not found.");
        }
    }

    void EnableCollider()
    {
        if (myCollider != null)
        {
            myCollider.enabled = true;
            childRenderer.sprite = unlockedSprite;
            childLock.SetActive(false);
        }
    }

    SpriteRenderer FindChildSpriteRenderer(string childName)
    {
        Transform childTransform = transform.Find(childName);
        if (childTransform != null)
        {
            return childTransform.GetComponent<SpriteRenderer>();
        }
        else
        {
            Debug.LogError($"Child with name {childName} not found.");
            return null;
        }
    }
}
