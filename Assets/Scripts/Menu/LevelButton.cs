using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour
{
    public int levelNumber;

    void OnMouseDown()
    {
        if(levelNumber>=0)
        {
            LoadSceneAndLevel(levelNumber);
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    void OnMouseEnter()
    {
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.8f);
    }

    void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
    }

    void LoadSceneAndLevel(int levelNum)
    {
        PlayerPrefs.SetInt("SelectedLevel", levelNum);
        SceneManager.LoadScene("GameplayScene");
    }
}