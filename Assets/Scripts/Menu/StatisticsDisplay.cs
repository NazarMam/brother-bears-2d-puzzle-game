using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StatisticsDisplay : MonoBehaviour
{
    public LevelSelectDatabaseManager databaseManager;
    public int LevelID;
    public string StatType;

    private TextMesh textMesh;

    void Start()
    {
        textMesh = GetComponent<TextMesh>();
        UpdateStatistics();
    }

    void UpdateStatistics()
    {
        int rotations = databaseManager.GetTotalRotations(LevelID);
        float bestTime = databaseManager.GetBestTime(LevelID);

        if(StatType=="Rotations")
            textMesh.text = $"{rotations}";

        else if(StatType=="BestTime")
            textMesh.text = $"{bestTime:F2}";
    }
}