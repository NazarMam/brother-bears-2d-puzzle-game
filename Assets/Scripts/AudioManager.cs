using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioClip musicClip;

    private AudioSource audioSource;
    public float volume = 1.0f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = musicClip;
        audioSource.loop = true;
        audioSource.Play();
    }

    void Update()
    {
        audioSource.volume = volume;
    }   

    void PauseGame()
    {
        audioSource.Pause();
    }

    void ResumeGame()
    {
        audioSource.UnPause();
    }

    void SwitchMusic(AudioClip newClip)
    {
        audioSource.Stop();
        audioSource.clip = newClip;
        audioSource.Play();
    }
}