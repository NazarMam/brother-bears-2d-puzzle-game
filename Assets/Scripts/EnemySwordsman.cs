using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swordsman : MonoBehaviour
{
    public float detectionRadius = 0.7f;
    public float nearDetectionRadius = 0.1f;
    public LayerMask playerLayer;
    public PlayerMovement playerMovement;
    public PlayerRotation playerRotationClass;
    float targetRotation;
    public VictoryScreen victoryScreen;
    public bool playerStatus=true; // 1 - Alive, 0 - Dead

    private bool isAnimating = false;
    private float animationDuration = 0.6f;
    private float jumpHeight = 1.0f;

    private void Update()
    {
        targetRotation = playerRotationClass.targetRotationAngle;

        DetectPlayerInDirection(Vector2.up);
        DetectPlayerInDirection(Vector2.down);
        DetectPlayerInDirection(Vector2.left);
        DetectPlayerInDirection(Vector2.right);
    }

    void DetectPlayerInDirection(Vector2 direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, detectionRadius, playerLayer);
        RaycastHit2D nearHit = Physics2D.Raycast(transform.position, direction, nearDetectionRadius, playerLayer);

        if (hit.collider != null)
        {
            bool playerInFront = false;
            bool nearPlayerInFront = false;
            if (targetRotation > 45 && targetRotation < 135)
            {
                // Player facing right
                playerInFront = (direction != Vector2.left);
                nearPlayerInFront = (direction == Vector2.left);
            }
            else if (targetRotation > 135 && targetRotation < 225)
            {
                // Player facing up
                playerInFront = (direction != Vector2.down);
                nearPlayerInFront = (direction == Vector2.down);
            }
            else if ((targetRotation > 225 && targetRotation < 315) || (targetRotation > -135 && targetRotation < -45))
            {
                // Player facing left
                playerInFront = (direction != Vector2.right);
                nearPlayerInFront = (direction == Vector2.right);
            }
            else if ((targetRotation > -45 && targetRotation < 45) || (targetRotation > 315 && targetRotation < 405))
            {
                // Player facing down
                playerInFront = (direction != Vector2.up);
                nearPlayerInFront = (direction == Vector2.up);
            }

            if (playerInFront && nearHit.collider == null && playerStatus==true)
            {
                Debug.DrawLine(transform.position, hit.point, Color.red);
                playerStatus=false;
                nearDetectionRadius = 0f;
                KillPlayer();
            }
            else if (nearPlayerInFront && nearHit.collider != null && nearDetectionRadius!=0)
            {
                Debug.DrawLine(transform.position, hit.point, Color.blue);
                KillEnemy();
            }
        }
    }

    void KillPlayer()
    {
        if (!isAnimating)
        {
            StartCoroutine(EnemyKillAnimation());
        }
    }

    IEnumerator EnemyKillAnimation()
    {
        isAnimating = true;
        playerMovement.enabled = false;

        Vector3 originalPosition = transform.position;
        Vector3 targetPosition = originalPosition + new Vector3(0f, jumpHeight, 0f);

        float elapsed = 0f;

        while (elapsed < animationDuration)
        {
            float t = elapsed / animationDuration;

            transform.position = Vector3.Lerp(originalPosition, targetPosition, Mathf.Sin(t * Mathf.PI * 0.5f));
            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPosition;
        yield return new WaitForSeconds(0.1f);
        elapsed = 0f;

        while (elapsed < animationDuration)
        {
            float t = elapsed / animationDuration;

            transform.position = Vector3.Lerp(targetPosition, originalPosition, Mathf.Sin(t * Mathf.PI * 0.5f));

            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.position = originalPosition;

        isAnimating = false;
        playerMovement.enabled = true;

        victoryScreen.ShowLossScreen();
    }

    void KillEnemy()
    {
        Destroy(gameObject);
    }
}