using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bowman : MonoBehaviour
{
    public float detectionRadius = 99f;
    private float nearDetectionRadius = 0.1f;
    public LayerMask playerLayer;
    public LayerMask wallLayer;
    public PlayerRotation playerRotationClass;
    public PlayerMovement playerMovement;
    public Swordsman enemySwordsman;
    Vector3 playerRotation; // Player sword-side rotation
    public VictoryScreen victoryScreen;

    private bool isAnimating = false;
    private float animationDuration = 1.0f;
    private float jumpHeight = 1.0f;

    private void Update()
    {
        playerRotation = playerMovement.playerRotation;

        DetectPlayerInDirection(Vector2.up);
        DetectPlayerInDirection(Vector2.down);
        DetectPlayerInDirection(Vector2.left);
        DetectPlayerInDirection(Vector2.right);
    }

    void DetectPlayerInDirection(Vector2 direction)
    {
        RaycastHit2D playerHit = Physics2D.Raycast(transform.position, direction, detectionRadius, playerLayer);
        RaycastHit2D wallHit = Physics2D.Raycast(transform.position, direction, detectionRadius, wallLayer);
        RaycastHit2D nearHit = Physics2D.Raycast(transform.position, direction, nearDetectionRadius, playerLayer);
        
        if (playerHit.collider != null)
        {

            if (wallHit.distance < playerHit.distance)
            {
                // Wall is closer than the player
                return;
            }

            bool playerInFront = false;
            bool nearPlayerInFront = false;
            if (playerRotation.z > 45 && playerRotation.z < 135)
            {
                // Player facing right
                playerInFront = (direction != Vector2.left);
                nearPlayerInFront = (direction == Vector2.left);
            }
            else if (playerRotation.z > 135 && playerRotation.z < 225)
            {
                // Player facing up
                playerInFront = (direction != Vector2.down);
                nearPlayerInFront = (direction == Vector2.down);
            }
            else if (playerRotation.z > 225 && playerRotation.z < 315)
            {
                // Player facing left
                playerInFront = (direction != Vector2.right);
                nearPlayerInFront = (direction == Vector2.right);
            }
            else if (playerRotation.z < 45)
            {
                // Player facing down
                playerInFront = (direction != Vector2.up);
                nearPlayerInFront = (direction == Vector2.up);
            }

            if (playerInFront && nearHit.collider == null && enemySwordsman.playerStatus == true)
            {
                enemySwordsman.playerStatus=false;
                nearDetectionRadius = 0f;
                KillPlayer();
            }
            else if (nearPlayerInFront && nearHit.collider != null && nearDetectionRadius!=0)
            {
                KillEnemy();
            }
        }
    }

    void KillPlayer()
    {
        if (!isAnimating)
        {
            StartCoroutine(EnemyKillAnimation());
        }
    }

    IEnumerator EnemyKillAnimation()
    {
        isAnimating = true;
        playerMovement.enabled = false;

        Vector3 originalPosition = transform.position;
        Vector3 targetPosition = originalPosition + new Vector3(0f, jumpHeight, 0f);

        float elapsed = 0f;

        while (elapsed < animationDuration)
        {
            float t = elapsed / animationDuration;

            transform.position = Vector3.Lerp(originalPosition, targetPosition, Mathf.Sin(t * Mathf.PI * 0.5f));

            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPosition;
        yield return new WaitForSeconds(0.1f);
        elapsed = 0f;

        while (elapsed < animationDuration)
        {
            float t = elapsed / animationDuration;

            transform.position = Vector3.Lerp(targetPosition, originalPosition, Mathf.Sin(t * Mathf.PI * 0.5f));

            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.position = originalPosition;

        isAnimating = false;
        playerMovement.enabled = true;
        victoryScreen.ShowLossScreen();
    }

    void KillEnemy()
    {
        Destroy(gameObject);
    }
}