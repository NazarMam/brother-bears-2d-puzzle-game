using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryScreen : MonoBehaviour
{
    public MapGenerator mapGenerator;
    public PlayerMovement playerMovement;
    public PlayerRotation playerRotation;
    public LevelSelectDatabaseManager levelSelectDatabaseManager;
    public Swordsman enemySwordsman;
    public GameObject victoryScreen;
    public GameObject lossScreen;
    private bool victoryScreenShowing = false;
    private bool lossScreenShowing = false;

    private void Start()
    {
        lossScreen.SetActive(false);
        victoryScreen.SetActive(false);
    }
    private void Update()
    {
        if (victoryScreenShowing && !lossScreenShowing)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                playerMovement.ResetPlayer();
                playerRotation.ResetRotation();
                SceneManager.LoadScene("MainMenu");
                enemySwordsman.playerStatus = true;
            }

            else if (Input.anyKeyDown)
            {
                LoadLevel(); 
                victoryScreenShowing = false;
                playerMovement.enabled = true;
                enemySwordsman.playerStatus = true;
                victoryScreen.SetActive(false);
            }
        }
        else if(lossScreenShowing && !victoryScreenShowing)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                playerMovement.ResetPlayer();
                playerRotation.ResetRotation();
                SceneManager.LoadScene("MainMenu");
                enemySwordsman.playerStatus = true;
            }

            else if (Input.anyKeyDown)
            {
                ReloadLevel(); 
                lossScreenShowing = false;
                playerMovement.enabled = true;
                enemySwordsman.playerStatus = true;
                lossScreen.SetActive(false);
            }
        }
    }

    public void ShowVictoryScreen()
    {
        victoryScreen.SetActive(true);
        victoryScreenShowing = true;
    }

    public void ShowLossScreen()
    {
        playerMovement.StopTimer();
        lossScreen.SetActive(true);
        lossScreenShowing = true;
    }

    public void LoadLevel()
    {
        mapGenerator.ResetLevel();
        playerMovement.ResetPlayer();
        playerRotation.ResetRotation();
    }

    public void ReloadLevel()
    {
        mapGenerator.ReloadLevel();
        playerMovement.ResetPlayer();
        playerRotation.ResetRotation();
    }

    public void StatReport(int levelID, bool completion, int currRotations, float currTime)
    {
        levelSelectDatabaseManager.UpdateLevelStatistics(levelID, completion, currRotations, currTime);
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerMovement.enabled = false;
            ShowVictoryScreen();
        }
    }
}
