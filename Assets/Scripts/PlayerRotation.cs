using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerRotation : MonoBehaviour
{
    public float rotationSpeed = 90f;
    private Transform playerTransform;
    public bool isRotating = false;
    public PlayerMovement playerMovement;
    public DatabaseManager databaseManager;
    public float targetRotationAngle=180f;
    public int rotations=0;
    public Text rotationText;

    void Start()
    {
        playerTransform = transform;
        playerTransform.rotation = Quaternion.Euler(0f, 0f, 180f);
        targetRotationAngle=180f;
    }

    void Update()
    {
        rotationText.text = rotations.ToString();

        if (!isRotating && !playerMovement.isMoving)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                isRotating = true;
                AttemptRotation(90);
                isRotating = false;
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                isRotating = true;
                AttemptRotation(-90);
                isRotating = false;
            }
        }
    }

    private void AttemptRotation(int degrees)
    {

        targetRotationAngle = Mathf.Round(playerTransform.eulerAngles.z + degrees);
        if ((targetRotationAngle % 90) != 0)
        {
            return;
        }

        if (!HasCollision(degrees, targetRotationAngle) && !HasDiagonalCollision(degrees, targetRotationAngle)) 
        {
            rotations++;
            StartCoroutine(RotateCoroutine(targetRotationAngle));
        } 
        else 
        {
            targetRotationAngle = Mathf.Round(playerTransform.eulerAngles.z);
            //databaseManager.OnInvalidRotate();
            //Debug.Log("Rotating " + degrees + " degrees was interrupted by a wall or a diagonal wall");
        }
    }

    private IEnumerator RotateCoroutine(float targetRotation)
    {
        float startRotation = playerTransform.eulerAngles.z;
        float t = 0;
        int degrees = Mathf.RoundToInt(targetRotation - startRotation);

        while (t < 1)
        {
            t += rotationSpeed * Time.deltaTime;
            float currentRotation = Mathf.LerpAngle(startRotation, targetRotation, t);

            Quaternion tempRotation = Quaternion.Euler(0f, 0f, currentRotation);
            playerTransform.rotation = tempRotation;

            yield return null;
        }

        playerTransform.rotation = Quaternion.Euler(0f, 0f, targetRotation);

        databaseManager.OnRotate();
    }

    private bool HasCollision(int degrees, float targetRotation)
    {
        Vector3 rotationVector = Quaternion.Euler(0f, 0f, degrees) * Vector3.up;
        float raycastDistance = 0.7f;

        int currentRotation = Mathf.RoundToInt(playerTransform.eulerAngles.z);
        float intendedRotation = (float)currentRotation + degrees;
        int direction = Mathf.RoundToInt(currentRotation - intendedRotation);

    
        RaycastHit2D hit1 = Physics2D.Raycast(playerTransform.position, Quaternion.Euler(0f, 0f, intendedRotation) * Vector3.up * -1, raycastDistance, LayerMask.GetMask("Walls"));

        if (hit1.collider != null)
        {
            StartCoroutine(BounceBack(direction));

            return true;
        }
        return false;
    }

    private IEnumerator BounceBack(int direction)
    {
        float startRotation = playerTransform.eulerAngles.z;
        float t = 0;

        while (t < 1)
        {
            t += rotationSpeed * Time.deltaTime;
            if(direction>0)
            {
                float currentRotation = Mathf.LerpAngle(startRotation, startRotation - 45, t);
                Quaternion tempRotation = Quaternion.Euler(0f, 0f, currentRotation);
                playerTransform.rotation = tempRotation;
            }
            else if(direction<0)
            {
                float currentRotation = Mathf.LerpAngle(startRotation, startRotation + 45, t);
                Quaternion tempRotation = Quaternion.Euler(0f, 0f, currentRotation);
                playerTransform.rotation = tempRotation;
            }

            yield return null;
        }

        playerTransform.rotation = Quaternion.Euler(0f, 0f, startRotation);
    }


    private bool HasDiagonalCollision(int degrees, float targetRotation)
    {
        float raycastDistance = Mathf.Sqrt(2f);
        int currentRotation = Mathf.RoundToInt(playerTransform.eulerAngles.z);
        float intendedRotation = (float)currentRotation + degrees;
        int direction = Mathf.RoundToInt(currentRotation - intendedRotation);

        RaycastHit2D hit;

        if(direction>0)
        {
            float raycastAngle = currentRotation - 45;
            Vector3 directionVector = Quaternion.Euler(0f, 0f, raycastAngle) * Vector3.up;
            hit = Physics2D.Raycast(playerTransform.position, -directionVector, raycastDistance, LayerMask.GetMask("Walls"));
        }
        else
        {
            float raycastAngle = currentRotation + 45;
            Vector3 directionVector = Quaternion.Euler(0f, 0f, raycastAngle) * Vector3.up;
            hit = Physics2D.Raycast(playerTransform.position, -directionVector, raycastDistance, LayerMask.GetMask("Walls"));
        }

        if (hit.collider != null)
        {
            StartCoroutine(BounceBack(direction));
            return true;
        }

        return false;
    }

    public void ResetRotation()
    {
        playerTransform.rotation = Quaternion.Euler(0f, 0f, 180f);
        targetRotationAngle=180f;
        rotations=0;
    }
}
